package br.com.neppo.exceptions.plugins.interfaces;

import br.com.neppo.exceptions.DynamicException;

public interface ExceptionHandler {

    DynamicException convert(Throwable t);

    /**
     * checks to see if a class is an child of `of`
     * @param point an exception's class
     * @param of an exception's class
     * @return true or false
     */
    static boolean instanceOf(Class<?> point, Class<?> of){

        if(point == null || of == null){
            return false;
        }

        return of.isAssignableFrom(point);
    }

    /**
     * checks to see if an object is an instance of `of`
     * @param point an exception
     * @param of an exception's class
     * @return true or false
     */
    static boolean instanceOf(Throwable point, Class<?> of){
        if(point == null || of == null){
            return false;
        }

        return of.isAssignableFrom(point.getClass());
    }

    /**
     * check to see whether or not the given exception has a an exception of type `of` in its cause stack.
     * @param exception
     * @param of
     * @return true or false
     */
    static boolean causeOf(Throwable exception, Class<?> of){

        if(exception == null || of == null){
            return false;
        }

        return of.equals(exception.getClass()) || (!exception.equals(exception.getCause()) && causeOf(exception.getCause(), of)) ;

    }

}